/**
 * Created by frobinet on 20/04/15.
 */

import java.io.{PrintWriter, FileOutputStream, BufferedReader, FileReader}
import java.util.{Random, Scanner}
import oscar.cp.core.CPSolver
import oscar.cp.constraints.Maximum
import scala.collection.mutable.Queue
import oscar.cp._
import oscar.visual._
import scala.collection.mutable.ArrayBuffer

object VRPTW extends App with CPModel {
  
  def sqr(x: Int) = x*x
  def distance(x: Int, y: Int, a: Int, b: Int) =
    (100 * Math.sqrt(sqr(x-a) + sqr(y-b))).floor.toInt
    

  // Open and read the integers contained in the whole file
  val filename = if(args.length > 0) args(0) else "instances/100/C104.txt"
  val content = Queue[Int]()
  val sc = new Scanner(new BufferedReader(new FileReader(filename)))
  while(sc.hasNext) {
    try {
      content += sc.next.toInt
    } catch {
      case exception: NumberFormatException =>
    }
  }
  sc.close()
  
  // Useful constants
  val rand = new Random()
  val nClients = (content.length - 2) / 7 - 1 // Number of customers
  val K = content.dequeue() // number of routes
  val n = nClients + K // number of visits
  val Q = content.dequeue() // Same capacity Q for every route
  val Clients = K until n
  val Depots = 0 until K
  val Visits = 0 until n


  /* DATA AND SETS DEFINITIONS */
  content.dequeue() // Skips number of the depot
  // Data for the unique depot, which will be duplicated m times for start and m times for end
  val (x0, y0, d0, etime0, ltime0, stime0) = (content.dequeue(), content.dequeue(), content.dequeue(), content.dequeue(), content.dequeue(), content.dequeue())
  val x = Array.fill(K){ x0 } ++ new Array[Int](nClients)  // x position for visit i
  val y = Array.fill(K){ y0 } ++ new Array[Int](nClients) // y position for visit i
  val D = Array.fill(K){ d0 } ++ new Array[Int](nClients) //  // Demand for visit i
  val Etime = Array.fill(K){ 100 * etime0 } ++ new Array[Int](nClients) // Earliest time for visit i
  val Ltime = Array.fill(K){ 100 * ltime0 } ++ new Array[Int](nClients) // Latest time for visit i
  val Stime = Array.fill(K){ 100 * stime0 } ++ new Array[Int](nClients) // Service time for client i

  for(i <- Clients) {
    content.dequeue() // Skip client number
    x(i) = content.dequeue()
    y(i) = content.dequeue()
    D(i) = content.dequeue()
    Etime(i) = 100 * content.dequeue()
    Ltime(i) = 100 * content.dequeue()
    Stime(i) = 100 * content.dequeue()
  }
  val C = Array.tabulate(x.length, x.length)((i, j) => distance(x(i), y(i), x(j), y(j))) // cost to travel from visits i and j  distance(i, j))
  
  /* VISUALIZATION CODE - taken and updated from an example for LINGI2266 */
  val visit = Array.fill(n,2)(0)
  for (i <- 0 until n) {
    visit(i)(0) = x(i)
    visit(i)(1) = y(i)
  }
  
  val cols = VisualUtil.getUniformColors(K)
  val frame = VisualFrame("VRP")
  val tour = VisualTour(visit.map(c => (c(0).toInt,c(1).toInt)))
  for (c <- 0 until K) {
    tour.nodeColor(c, cols(c))
    tour.nodeRadius(c, 1)
  }
  for (c <- K  until n) {
    tour.nodeRadius(c, 1)
  }
  frame.createFrame("VRP Tour").add(tour)
  frame.pack()  
  
  // Updates the visualization  
  def updateTour(succ: Seq[CPIntVar],truck: Seq[CPIntVar]): Unit = {
    for (i <- 0 until n) {
      tour.nodeColor(i,cols(truck(i).value))
      tour.edgeDest(i,succ(i).value)
      tour.edgeColor(i,cols(truck(i).value))
    }
    
    tour.repaint()
  }
  
  /* VARIABLES */
  // Routes and loads
  val r = Array.fill(n)(CPIntVar(Depots)) // routes
  val q = Array.fill(K)(CPIntVar(0 to Q)) // loads
  
  // Successors and predecessors
  val s = Array.fill(n)(CPIntVar(Visits)) // successors
  val p = Array.fill(n)(CPIntVar(Visits)) // predecessors
  
  // Time variables
  val a = Array.tabulate(n)(i => CPIntVar(0 to Ltime(i)))
  val t = Array.tabulate(n)(i => CPIntVar(Etime(i) to Ltime(i)))
  val d = Array.tabulate(n)(i => CPIntVar(Etime(i)+Stime(i) to Ltime(i)+Stime(i)))
  
  // Objective
  val totalCost = sum(0 until n)(i => C(i)(s(i)))
  
  
  /* CONSTRAINTS */
  // Loads associated to each route
  add(binPacking(r, D, q)) 
  
  // Successors and predecessors are the opposite of each other
  add(inverse(s, p))
  
  // successors form a circuit (and so do predecessors)
  add(circuit(s)) 
  
  // Force a route for each depot
  for (i <- Depots) {
	  add(r(i) == i)
  }
  
  // Force the route of successors to be the same
  for (i <- Clients) {
    add(r(i) == r(s(i)))
  }
  
  // Constraints associated to the time windows
  // arrival to successor after departure from predecessor + travel time
  for (i <- Visits) {
   	add(a(s(i)) == d(i) + C(i)(s(i)))
  }
  
  for (i <- Clients) {
    add(t(i) >= a(i)) // Can arrive in advance
	  add(d(i) == t(i) + Stime(i)) // Departure immediately after service
  }
  
  
  /* SEARCH */
  // Minimize the objective
  minimize(totalCost)
  
  // Variables to store best solution found so far
  var bestR = Array.fill(n)(0)
  var bestS = Array.fill(n)(0)
  var bestP = Array.fill(n)(0)
  var bestC = Int.MaxValue // Current minimal distance
  var solFound = false
  
  onSolution { 
    solFound = true
    // Remember the current best solution
    bestR = r map { _.value }
    bestS = s map { _.value }
    bestP = p map { _.value }
    bestC = totalCost.value
    updateTour(s, r) // Update the visualization
  }
  
  search(binaryFirstFail(p, i => closestPredecessor(i, C(p.indexOf(i)))))
  
  // Simple branch and bound to find a single admissible solution
  var time = System.currentTimeMillis();
  println("Looking for an initial solution...")
  start(nSols=1, timeLimit=58000)
  
  // LNS to improve the first solution
  val relaxed = 20.0 
  var iter = 1
  while ((System.currentTimeMillis() - time) < 58000) { // keep going for max 60s
    println("ITER: " + iter)
    val fails = if(iter > 500) 100 else if(iter > 100) 50 else 20
	val stat = startSubjectTo(failureLimit = fails) {
	    // Print current solution
	    val usedRoutes = (for(i <- Clients) yield bestR(i)).toSet.size
	    println("\tCost: " + bestC)
	    println("\tRoutes used: " + usedRoutes)

	    // Randomly choose which variables to freeze 
	    // and freeze them to their value in the current solution
	    val frozen = Visits filter { i => rand.nextInt(100) > relaxed }
	    add(frozen map { i => p(i) == bestP(i) })
	}
    iter += 1
  }
   
  /* OUTPUT RESULT */
  val out = new PrintWriter(new FileOutputStream("output"))
  writeOutput(bestS, bestR, bestC, out)
  out.close()
  //frame.setVisible(false)
  //frame.dispose()
  
  
  /* Helper methods */
  def closestPredecessor(s: CPIntVar, costs: Array[Int]): Int = {
    var minCost = Int.MaxValue
    var minIndex = 0
    for (succ <- s) {
      val c = costs(succ)
      if (c < minCost) {
        minCost = c
        minIndex = succ
      }
    }
    return minIndex
  }
 
  // Outputs the results (inspired by a similar technique from Goeric Huybrechts)  
  def writeOutput (s: Array[Int], r: Array[Int], cost: Int, out: PrintWriter) : Unit = {
      out.println(cost)
      val routesUsed = (for (i <- Clients) yield r(i)).toSet.size
      out.println(routesUsed)
    
      for(i <- 0 until K) {
        var index = i
        var count = 0
        var clients = ArrayBuffer[Int]()
        
        while(s(index) >= K) {
          var client = s(index) - K +1
          clients += client
          index = s(index)
          count += 1
        }
        
        if (count != 0) {
          out.print(count + " ")
          out.println(clients mkString " ")
        }
      }
   }
  
}